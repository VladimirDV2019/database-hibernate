package com.aendi;

import com.aendi.dao.*;
import org.flywaydb.core.Flyway;

import java.util.List;
import java.util.Set;

public class Application {
    public static void main(String[] args) throws Exception {
        String url = "jdbc:postgresql://localhost:5432/hibernate-example";
        String user = "postgres";
        String password = "";
        Flyway flyway = Flyway.configure().dataSource(url, user, password).load();
        flyway.migrate();
        StudentEntity studentEntity = new StudentEntity();
        StudentEntity studentEntity2 = new StudentEntity();
        StudentRankEntity studentRankEntity = new StudentRankEntity();
        StudentRankEntity studentRankEntity2 = new StudentRankEntity();
        BookEntity bookEntity1 = new BookEntity();
        BookEntity bookEntity2 = new BookEntity();
        GroupEntity groupEntity1 = new GroupEntity();
        GroupEntity groupEntity2 = new GroupEntity();
        groupEntity1.setStudents(List.of(studentEntity));
        groupEntity2.setStudents(List.of(studentEntity));
        groupEntity1.setName("YA-22");
        groupEntity2.setName("NF-14");
        studentEntity.setFirstName("Alex");
        studentEntity.setSecondName("Petrov");

        studentRankEntity.setStudentRank(56);
        studentEntity.setStudentRankEntity(studentRankEntity);
        studentEntity.setStudentGroups(Set.of(groupEntity1, groupEntity2));
        StudentDao studentDao = new StudentDao();
        studentDao.create(studentEntity);

        studentEntity2.setFirstName("Vladimir");
        studentEntity2.setSecondName("Miromar");
        studentEntity2.setStudentRankEntity(studentRankEntity2);
//        studentEntity2.setStudentGroups(Set.of(groupEntity2));
        studentRankEntity2.setStudentRank(99);
        studentDao.create(studentEntity2);

        StudentEntity bySecondName = studentDao.getBySecondName("Petrov");
        bySecondName.setFirstName("Andrew");
        bySecondName.setSecondName("Perenov");
        bookEntity1.setAuthor("Landau");
        bookEntity1.setTopic("Physics and magnetism");
        bookEntity1.setStudent(bySecondName);
        bySecondName.setBookEntitySet(Set.of(bookEntity1));
        bySecondName.getStudentGroups();
        studentDao.update(bySecondName);

        System.out.println(1);
    }
}
