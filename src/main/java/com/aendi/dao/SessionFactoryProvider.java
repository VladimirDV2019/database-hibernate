package com.aendi.dao;

import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

import java.util.Properties;

import static org.hibernate.cfg.AvailableSettings.*;

public class SessionFactoryProvider {
    private static SessionFactory sessionFactory;

    public static SessionFactory getSessionFactory() {
        if (sessionFactory == null) {
            try {
                Configuration configuration = new Configuration();
                Properties settings = new Properties();
                settings.put(DRIVER, "org.postgresql.Driver");
                settings.put(URL, "jdbc:postgresql://localhost:5432/hibernate-example");
                settings.put(USER, "postgres");
                settings.put(PASS, "");
                settings.put(DIALECT, "org.hibernate.dialect.MySQL5Dialect");
                settings.put(SHOW_SQL, "true");
                configuration.setProperties(settings);
                configuration.addAnnotatedClass(StudentRankEntity.class);
                configuration.addAnnotatedClass(StudentEntity.class);
                configuration.addAnnotatedClass(GroupEntity.class);
                configuration.addAnnotatedClass(BookEntity.class);
                ServiceRegistry serviceRegistry =
                        new StandardServiceRegistryBuilder().applySettings(configuration.getProperties())
                                .build();
                sessionFactory = configuration.buildSessionFactory(serviceRegistry);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
        return sessionFactory;
    }
}
