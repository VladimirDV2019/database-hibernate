package com.aendi.dao;

import org.hibernate.Session;
import org.hibernate.query.Query;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

public class StudentDao {
    public StudentEntity getById(Long id) throws Exception {
        try (Session session = SessionFactoryProvider.getSessionFactory().openSession()) {
            StudentEntity studentEntity = session.get(StudentEntity.class, id);
            if (studentEntity == null) throw new Exception("Cannot fetch student with id: " + id + " from database");
            return studentEntity;
        } catch (Exception e) {
            throw new Exception("Cannot fetch student with id: " + id + " from database", e);
        }
    }

    public StudentEntity getBySecondName(String secondName) throws Exception {
        try (Session session = SessionFactoryProvider.getSessionFactory().openSession()) {
            Query<StudentEntity> query = session.createQuery("SELECT a FROM StudentEntity a where a.secondName=:secondName", StudentEntity.class);
            query.setParameter("secondName", secondName);
            if (query.getResultList().get(0) == null)
                throw new Exception("Cannot fetch student with name: " + secondName + " from database");
            return query.getResultList().get(0);
        } catch (Exception e) {
            throw new Exception("Cannot fetch student with name: " + secondName + " from database", e);
        }
    }

    public StudentEntity getByRank(Integer rank) throws Exception {
        try (Session session = SessionFactoryProvider.getSessionFactory().openSession()) {
            Query<StudentEntity> query = session.createQuery("SELECT a FROM StudentEntity a inner join StudentRankEntity b on b.id=a.studentRankEntity where b.studentRank=:rank ", StudentEntity.class);
            query.setParameter("rank", rank);
            return query.getResultList().get(0);
        } catch (
                Exception e) {
            throw new Exception("Cannot fetch student with rank: " + rank + " from database", e);
        }
    }

    public List<StudentEntity> getAll() throws Exception {
        try (Session session = SessionFactoryProvider.getSessionFactory().openSession()) {
            return session.createQuery("SELECT a FROM StudentEntity a", StudentEntity.class).getResultList();
        } catch (Exception e) {
            throw new Exception("Cannot fetch students from database", e);
        }
    }

    public void create(StudentEntity studentEntity) throws Exception {
        try (Session session = SessionFactoryProvider.getSessionFactory().openSession()) {
            try {
                session.beginTransaction();
                GroupDao groupDao = new GroupDao();
                Set<GroupEntity> studentGroups = new HashSet<>(studentEntity.getStudentGroups());
                for (GroupEntity groupEntity : studentEntity.getStudentGroups()) {
                    try {
                        GroupEntity byName = groupDao.getByName(groupEntity.getName());
                        studentGroups.remove(groupEntity);
                        byName.setStudentCount(byName.getStudentCount() + 1);
                        studentGroups.add(byName);
                        session.merge(byName);
                    } catch (Exception e) {
                        Logger.getAnonymousLogger().info(e.getMessage());
                        session.persist(groupEntity);
                    }
                }
                studentEntity.setStudentGroups(studentGroups);
                session.persist(studentEntity);
                session.getTransaction().commit();
            } catch (Exception e) {
                session.getTransaction().rollback();
                throw new Exception("Cannot store student to database", e);
            }
        }
    }

    public void update(StudentEntity studentEntity) throws Exception {
        try (Session session = SessionFactoryProvider.getSessionFactory().openSession()) {
            try {
                session.beginTransaction();
                session.merge(studentEntity);
                session.getTransaction().commit();
            } catch (Exception e) {
                session.getTransaction().rollback();
                throw new Exception("Cannot update student", e);
            }
        }
    }

    public void delete(StudentEntity studentEntity) throws Exception {
        try (Session session = SessionFactoryProvider.getSessionFactory().openSession()) {
            try {
                session.beginTransaction();
                session.delete(studentEntity);
                session.getTransaction().commit();
            } catch (Exception e) {
                session.getTransaction().rollback();
                throw new Exception("Cannot delete student from database", e);
            }
        }
    }
}
