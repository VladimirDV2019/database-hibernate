package com.aendi.dao;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@Entity
@Table(schema = "public", name = "student")
public class StudentEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "first_name")
    private String firstName;
    @Column(name = "second_name")
    private String secondName;
    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "student_rank_id", nullable = false)
    private StudentRankEntity studentRankEntity;
    @OneToMany(mappedBy = "student",
            fetch = FetchType.EAGER,
            cascade = {CascadeType.PERSIST, CascadeType.MERGE},
            orphanRemoval = true)
    private Set<BookEntity> bookEntitySet = new HashSet<>();
    @ManyToMany(fetch = FetchType.EAGER,
            cascade =  CascadeType.MERGE)
    @JoinTable(name = "student_group_mapping",
            joinColumns = @JoinColumn(name = "student_id"),
            inverseJoinColumns = @JoinColumn(name = "group_id"))
    private Set<GroupEntity> studentGroups = new HashSet<>();
}
