package com.aendi.dao;

import org.hibernate.Session;

import java.util.List;

public class StudentRankDao {
    public StudentRankEntity getById(Long id) throws Exception {
        try (Session session = SessionFactoryProvider.getSessionFactory().openSession()) {
            StudentRankEntity studentRankEntity = session.get(StudentRankEntity.class, id);
            if (studentRankEntity == null)
                throw new Exception("Cannot fetch student rank with id: " + id + " from database");
            return studentRankEntity;
        } catch (Exception e) {
            throw new Exception("Cannot fetch student rank with id: " + id + " from database", e);
        }
    }

    public List<StudentRankEntity> getAll() throws Exception {
        try (Session session = SessionFactoryProvider.getSessionFactory().openSession()) {
            return session.createQuery("SELECT a FROM StudentRankEntity a", StudentRankEntity.class).getResultList();
        } catch (Exception e) {
            throw new Exception("Cannot fetch student's ranks from database", e);
        }
    }

    public void create(StudentRankEntity studentRankEntity) throws Exception {
        try (Session session = SessionFactoryProvider.getSessionFactory().openSession()) {
            try {
                session.beginTransaction();
                session.persist(studentRankEntity);
                session.getTransaction().commit();
            } catch (Exception e) {
                session.getTransaction().rollback();
                throw new Exception("Cannot store student rank to database", e);
            }
        }
    }

    public void update(StudentRankEntity studentRankEntity) throws Exception {
        try (Session session = SessionFactoryProvider.getSessionFactory().openSession()) {
            try {
                session.beginTransaction();
                session.merge(studentRankEntity);
                session.getTransaction().commit();
            } catch (Exception e) {
                session.getTransaction().rollback();
                throw new Exception("Cannot update student rank", e);
            }
        }
    }

    public void delete(StudentRankEntity studentRankEntity) throws Exception {
        try (Session session = SessionFactoryProvider.getSessionFactory().openSession()) {
            try {
                session.beginTransaction();
                session.delete(studentRankEntity);
                session.getTransaction().commit();
            } catch (Exception e) {
                session.getTransaction().rollback();
                throw new Exception("Cannot delete student rank from database", e);
            }
        }
    }
}
