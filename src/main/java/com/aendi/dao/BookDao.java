package com.aendi.dao;

import org.hibernate.Session;

import java.util.List;

public class BookDao {
    public BookEntity getById(Long id) throws Exception {
        try (Session session = SessionFactoryProvider.getSessionFactory().openSession()) {
            BookEntity bookEntity = session.get(BookEntity.class, id);
            if (bookEntity == null) throw new Exception("Cannot fetch book with id: " + id + " from database");
            return bookEntity;
        } catch (Exception e) {
            throw new Exception("Cannot fetch book with id: " + id + " from database", e);
        }
    }

    public List<BookEntity> getAll() throws Exception {
        try (Session session = SessionFactoryProvider.getSessionFactory().openSession()) {
            return session.createQuery("SELECT a FROM BookEntity a", BookEntity.class).getResultList();
        } catch (Exception e) {
            throw new Exception("Cannot fetch books from database", e);
        }
    }

    public void create(BookEntity bookEntity) throws Exception {
        try (Session session = SessionFactoryProvider.getSessionFactory().openSession()) {
            try {
                session.beginTransaction();
                session.persist(bookEntity);
                session.getTransaction().commit();
            } catch (Exception e) {
                session.getTransaction().rollback();
                throw new Exception("Cannot store book to database", e);
            }
        }
    }

    public void delete(BookEntity bookEntity) throws Exception {
        try (Session session = SessionFactoryProvider.getSessionFactory().openSession()) {
            try {
                session.beginTransaction();
                session.delete(bookEntity);
                session.getTransaction().commit();
            } catch (Exception e) {
                session.getTransaction().rollback();
                throw new Exception("Cannot delete book from database", e);
            }
        }
    }

}
