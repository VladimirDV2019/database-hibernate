package com.aendi.dao;

import org.hibernate.Session;
import org.hibernate.query.Query;

import java.util.List;

public class GroupDao {
    public GroupEntity getById(Long id) throws Exception {
        try (Session session = SessionFactoryProvider.getSessionFactory().openSession()) {
            GroupEntity groupEntity = session.get(GroupEntity.class, id);
            if (groupEntity == null)
                throw new Exception("Cannot fetch group with id: " + id + " from database");
            return groupEntity;
        } catch (Exception e) {
            throw new Exception("Cannot fetch group with id: " + id + " from database", e);
        }
    }

    public GroupEntity getByName(String name) throws Exception {
        try (Session session = SessionFactoryProvider.getSessionFactory().openSession()) {
            Query<GroupEntity> query = session.createQuery("SELECT a FROM GroupEntity a where a.name=:name", GroupEntity.class);
            query.setParameter("name", name);
            if (query.getResultList().get(0) == null)
                throw new Exception("Cannot fetch group with name: " + name + " from database");
            return query.getResultList().get(0);
        } catch (Exception e) {
            throw new Exception("Cannot fetch group with name: " + name + " from database", e);
        }
    }

    public List<GroupEntity> getAll() throws Exception {
        try (Session session = SessionFactoryProvider.getSessionFactory().openSession()) {
            return session.createQuery("SELECT a FROM GroupEntity a", GroupEntity.class).getResultList();
        } catch (Exception e) {
            throw new Exception("Cannot fetch groups from database", e);
        }
    }

    public void create(GroupEntity groupEntity) throws Exception {
        try (Session session = SessionFactoryProvider.getSessionFactory().openSession()) {
            try {
                session.beginTransaction();
                session.persist(groupEntity);
                session.getTransaction().commit();
            } catch (Exception e) {
                session.getTransaction().rollback();
                throw new Exception("Cannot store group to database", e);
            }
        }
    }

    public void update(GroupEntity groupEntity) throws Exception {
        try (Session session = SessionFactoryProvider.getSessionFactory().openSession()) {
            try {
                session.beginTransaction();
                session.merge(groupEntity);
                session.getTransaction().commit();
            } catch (Exception e) {
                session.getTransaction().rollback();
                throw new Exception("Cannot update group", e);
            }
        }
    }

    public void delete(GroupEntity groupEntity) throws Exception {
        try (Session session = SessionFactoryProvider.getSessionFactory().openSession()) {
            try {
                session.beginTransaction();
                session.delete(groupEntity);
                session.getTransaction().commit();
            } catch (Exception e) {
                session.getTransaction().rollback();
                throw new Exception("Cannot delete group from database", e);
            }
        }
    }
}
