CREATE TABLE IF NOT EXISTS "student_rank"
(
    id         SERIAL PRIMARY KEY,
    student_rank INT
);
CREATE TABLE IF NOT EXISTS "student"
(
    id         SERIAL PRIMARY KEY,
    first_name  VARCHAR(50)  NOT NULL,
    second_name  VARCHAR(50)  NOT NULL,
    student_rank_id BIGINT UNIQUE,
    constraint "student_rank_id_fkey" foreign key (student_rank_id) references "student_rank" (id)
);
CREATE TABLE IF NOT EXISTS "book"
(
    id         SERIAL PRIMARY KEY,
    author  VARCHAR(100)  NOT NULL,
    topic   VARCHAR(100)  NOT NULL,
    student_id BIGINT,
    constraint "student_id_fkey" foreign key (student_id) references "student" (id)

);
CREATE TABLE IF NOT EXISTS "group"
(
    id         SERIAL PRIMARY KEY,
    name  VARCHAR(50) UNIQUE NOT NULL,
    student_count INT
);
CREATE TABLE IF NOT EXISTS "student_group_mapping"
(
    student_id BIGINT NOT NULL,
    group_id BIGINT NOT NULL,
    constraint "student_id_fkey" foreign key (student_id) references "student" (id),
    constraint "group_id_fkey" foreign key (group_id) references "group" (id)
);